export interface Race {
  Circuit: Circuit;
  Results: RaceResult[];
  date: string;
  raceName: string;
  round: string;
  season: string;
  time: string;
  url: string;
}

export interface Circuit {
  Location: Location;
  circuitId: string;
  circuitName: string;
  url: string;
}

export interface Location {
  country: string;
  lat: string;
  locality: string;
  long: string;
}

export interface RaceResult {
  Constructor: Constructor;
  Driver: Driver;
  Time: { millis: string, time: string };
  grid: string;
  laps: string;
  number: string;
  points: string;
  position: string;
  positionText: string;
  status: string;
}

export interface Driver {
  code: string;
  dateOfBirth: string;
  driverId: string;
  familyName: string;
  givenName: string;
  nationality: string;
  permanentNumber: string;
  url: string;
  isSessionWinner: boolean;
}

export interface Constructor {
  constructorId: string;
  url: string;
  name: string;
  nationality: string;
}
