import {ApiResult} from './api-result.model';
import {TestBed} from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FontModule} from '../../modules/common/font/font.module';

describe('ApiResult', () => {
  let component: ApiResult<{}>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FontModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
    component = new ApiResult<{}>();
  });

  it('setLoading should trigger loading, empty error message and trigger hasError', () => {
    component.isLoading = true;
    component.setLoading(false);
    expect(component.isLoading).toBeFalse();
  });
});
