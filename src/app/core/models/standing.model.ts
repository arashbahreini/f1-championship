import {RaceResult} from './race.model';

export interface Standing {
  DriverStandings: RaceResult[];
  round: string;
  season: string;
}
