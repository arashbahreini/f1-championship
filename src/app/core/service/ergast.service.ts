import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Season} from '../models/season.model';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Race} from '../models/race.model';
import {Standing} from '../models/standing.model';

@Injectable({
  providedIn: 'root'
})
export class ErgastService {

  constructor(private httpClient: HttpClient) {
  }

  getSeasons(): Observable<Season[]> {
    return this.httpClient.get<Season[]>('http://ergast.com/api/f1/seasons.json?limit=10&offset=55')
      .pipe(map((x: any) => {
        return x.MRData.SeasonTable.Seasons as Season[];
      }));
  }

  getRaces(season: string): Observable<Race[]> {
    return this.httpClient.get<Race[]>(`http://ergast.com/api/f1/${season}/results/1.json`)
      .pipe(map((result: any) => {
        return result.MRData.RaceTable.Races;
      }));
  }

  getWinners(): Observable<Standing[]> {
    return this.httpClient.get<Standing[]>(`http://ergast.com/api/f1/driverstandings/1.json?limit=30&offset=55`)
      .pipe(map((result: any) => {
        return result.MRData.StandingsTable.StandingsLists;
      }));
  }
}
