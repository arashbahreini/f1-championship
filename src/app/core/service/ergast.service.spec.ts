import {TestBed} from '@angular/core/testing';

import {ErgastService} from './ergast.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import {Season} from '../models/season.model';
import {Race} from '../models/race.model';
import {Standing} from '../models/standing.model';

describe('ErgastService', () => {
  let service: ErgastService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ErgastService);
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ErgastService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getSeasons should return Observable<Season[]>', () => {
    const fakeResult = {
      MRData: {
        SeasonTable: {
          Seasons: [] as Season[]
        }
      }
    };
    httpClientSpy.get.and.returnValue(of(fakeResult));
    service.getSeasons().subscribe(res => {
      expect(res).toEqual([] as Season[]);
    });
  });

  it('getRaces should return Observable<Race[]>', () => {
    const fakeResult = {
      MRData: {
        RaceTable: {
          Races: [] as Race[]
        }
      }
    };
    httpClientSpy.get.and.returnValue(of(fakeResult));
    service.getRaces('').subscribe(res => {
      expect(res).toEqual([] as Race[]);
    });
  });

  it('getWinners should return Observable<Standing[]>', () => {
    const fakeResult = {
      MRData: {
        StandingsTable: {
          StandingsLists: [] as Standing[]
        }
      }
    };

    httpClientSpy.get.and.returnValue(of(fakeResult));
    service.getWinners().subscribe(res => {
      expect(res).toEqual([] as Standing[]);
    });
  });
});
