import { NgModule } from '@angular/core';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faArrowLeft, faExternalLinkAlt, faFlagCheckered, faHome, faMapSigns, faSync} from '@fortawesome/free-solid-svg-icons';

@NgModule({
  declarations: [],
  imports: [ FontAwesomeModule ],
  exports: [ FontAwesomeModule ]
})
export class FontModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFlagCheckered, faMapSigns, faArrowLeft, faSync, faExternalLinkAlt, faHome);
  }
}
