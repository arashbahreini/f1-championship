import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NavigationEnd, Router} from '@angular/router';


@Component({
  selector: 'app-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.scss'],
  animations: [
    trigger('greetingAnimation', [
      transition('true <=> false', animate('.5s')),
      state('true', style({
        paddingTop: '1.5vh',
        color: '#878787',
        backgroundColor: 'rgba(135, 135, 135, .2)'
      })),
      state('false', style({
        paddingTop: '20vh',
        color: '#759d01',
      })),
    ]),
  ]
})
export class GreetingComponent implements OnInit {
  @Output() isFinished: EventEmitter<boolean> = new EventEmitter<boolean>();

  headerMessage: string;
  headerText: string;
  isGreetingInProgressFinished = 'false';
  intervalId: number[] = [];
  timeoutId: number[] = [];
  stopRunAnimation: boolean;

  constructor(private route: Router) {

  }

  ngOnInit(): void {
    // Have below 3 lines to run animation when page refresh/open at the first time
    this.showBlinker();
    this.headerText = this.generateHeaderText();
    this.stopRunAnimation = false;

    // This route event is responsible to show and animate new header per route change
    this.route.events.subscribe((value: NavigationEnd) => {
        if (value instanceof NavigationEnd) {
          this.intervalId.forEach(element => {
            clearInterval(element);
          });
          this.intervalId = [];
          if (!this.stopRunAnimation) {
            this.showBlinker();
          }
          this.headerText = this.generateHeaderText();
          this.stopRunAnimation = false;
        }
      }
    );
  }

  // To fill the header text
  generateHeaderText(): string {
    const currentUrl = this.route.url;
    if (currentUrl.indexOf('winners') > -1) {
      return 'List of winners';
    } else {
      return 'Welcome to greatest F1 championship world';
    }
  }

  // Check if user is in the root page, return true otherwise return false
  showGreeting(): boolean {
    return this.route.url === '/';
  }

  showBackButton(): boolean {
    return this.route.url !== '/' && this.route.url !== '/home';
  }

  // Show blinker `| | | |` in the main page before typing welcome text
  showBlinker(): void {
    // Adding + to pars interval as number
    this.intervalId.push(+setInterval(() => {
      this.headerMessage = this.headerMessage === '|' ? '' : '|';
    }, 200));
    setTimeout(() => {
      clearInterval(this.intervalId[this.intervalId.length - 1]);
      this.headerMessage = '';
      this.presentHeaderMessage(this.headerText);
    }, 1000);
  }

  // When animation finish navigate to /home to present the seasons
  animationFinish(event): void {
    if (event.toState === 'true') {
      this.route.navigateByUrl('/home');
      this.stopRunAnimation = true;
    }
  }

  // Type greeting message with setInterval function, then user feels that the text is
  // appearing like somebody is typing it
  presentHeaderMessage(message: string): void {
    // Added + to convert interval to number
    this.intervalId.push(+setInterval(() => {
      if (this.headerMessage.length === message.length) {
        clearInterval(this.intervalId[1]);
        this.isGreetingInProgressFinished = 'true';
        return;
      }
      this.headerMessage += message[this.headerMessage.length];
    }, 30));
  }

}
