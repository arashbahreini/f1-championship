import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GreetingComponent} from './greeting.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FontModule} from '../../common/font/font.module';
import {of} from 'rxjs';
import {ActivatedRoute, NavigationEnd, Router, RouterModule} from '@angular/router';

class MockRouter {
  events = of(new NavigationEnd(0, 'winners', 'winners'));
  url = '';

  navigateByUrl(url: string): void {

  }
}

describe('GreetingComponent', () => {
  let component: GreetingComponent;
  let fixture: ComponentFixture<GreetingComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule, BrowserAnimationsModule, FontModule],
      declarations: [GreetingComponent],
      providers: [
        {provide: Router, useClass: MockRouter},
        {provide: ActivatedRoute, useValue: {}}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreetingComponent);
    component = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('showBlinker should be called on onInit and route change', () => {
    spyOn(component, 'showBlinker');
    component.ngOnInit();
    expect(component.showBlinker).toHaveBeenCalled();
  });

  it('presentGreetingMessage should be called after showBlinker', () => {
    jasmine.clock().install(); // first install the clock
    spyOn(component, 'presentHeaderMessage');
    component.showBlinker();
    jasmine.clock().tick(1500); // wait for 1500ms
    expect(component.presentHeaderMessage).toHaveBeenCalled();
    jasmine.clock().uninstall(); // uninstall the clock when test is done
  });

  it('presentGreetingMessage should put 1 character to greetingMessage every X milli second', () => {
    component.headerMessage = '';
    jasmine.clock().install();
    component.presentHeaderMessage('B');
    jasmine.clock().tick(30);
    expect(component.headerMessage).toBe('B');
    jasmine.clock().uninstall();
  });

  it('greetingMessage should be empty after 1000ms when showBlinker get called', () => {
    jasmine.clock().install();
    component.showBlinker();
    jasmine.clock().tick(1000);
    expect(component.headerMessage).toBe('');
    jasmine.clock().uninstall();
  });

  it('isGreetingInProgressFinished should be true if this.headerMessage.length === message.length', () => {
    component.isGreetingInProgressFinished = 'false';
    component.headerMessage = 'a';
    jasmine.clock().install();
    component.presentHeaderMessage('a');
    jasmine.clock().tick(40);
    expect(component.isGreetingInProgressFinished).toEqual('true');
    jasmine.clock().uninstall();
  });

  it('generateHeaderText should return appropriate header', () => {
    // @ts-ignore: force this private property value for testing.
    // Using ts-ignore to change read-only value of route.url
    // router.url has become read-only since version 9 of Angular
    router.url = 'winners';
    let headerText = component.generateHeaderText();
    expect(headerText).toBe('List of winners');

    // @ts-ignore: force this private property value for testing.
    router.url = 'something else';
    headerText = component.generateHeaderText();
    expect(headerText).toBe('Welcome to greatest F1 championship world');
  });

  it('When animation finish, should navigate to /home page', () => {
    spyOn(router, 'navigateByUrl');
    component.animationFinish({toState: 'true'});
    expect(router.navigateByUrl).toHaveBeenCalledWith('/home');
  });
});
