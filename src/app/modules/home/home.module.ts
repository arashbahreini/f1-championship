import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import { GreetingComponent } from './greeting/greeting.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { SeasonsComponent } from './seasons/seasons.component';



@NgModule({
  declarations: [HomeComponent, GreetingComponent, SeasonsComponent],
  imports: [
    HomeRoutingModule,
    CommonModule,
    FontAwesomeModule
  ]
})
export class HomeModule { }
