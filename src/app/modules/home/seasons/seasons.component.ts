import {Component, OnInit} from '@angular/core';
import {Season} from '../../../core/models/season.model';
import {ErgastService} from '../../../core/service/ergast.service';
import {faWikipediaW} from '@fortawesome/free-brands-svg-icons';
import {ApiResult} from '../../../core/models/api-result.model';

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.scss']
})
export class SeasonsComponent implements OnInit {
  seasons: ApiResult<Season[]> = new ApiResult<Season[]>();

  constructor(private ergastService: ErgastService) {
  }

  ngOnInit(): void {
    this.getSeasons();
  }

  // Get list of seasons
  getSeasons(): void {
    this.ergastService.getSeasons().subscribe((res: Season[]) => {
      this.seasons.setData(res);
    }, () => {
      this.seasons.setError('Failed to load sessions');
    });
  }

  navigateOut(season: Season): void {
    // I decide to open new tab here, then user does
    // not loose his focus from my website
    window.open(season.url, '_blank');
  }
}
