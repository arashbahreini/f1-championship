import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SeasonsComponent} from './seasons.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FontModule} from '../../common/font/font.module';
import {Observable, of, throwError} from 'rxjs';
import {Season} from '../../../core/models/season.model';
import {ErgastService} from '../../../core/service/ergast.service';
import {Mock} from 'protractor/built/driverProviders';

class MockErgastService {
  getSeasons(): Observable<Season[]> {
    return of([]);
  }
}

describe('SeasonsComponent', () => {
  let component: SeasonsComponent;
  let fixture: ComponentFixture<SeasonsComponent>;
  let service: MockErgastService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FontModule],
      providers: [
        {
          provide: ErgastService,
          useClass: MockErgastService
        }
      ],
      declarations: [SeasonsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(ErgastService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getSeasons should be called in OnInit', () => {
    spyOn(component, 'getSeasons');
    component.ngOnInit();
    expect(component.getSeasons).toHaveBeenCalled();
  });

  it('ergastService.getSession service should be called in getSessions method', () => {
    spyOn(service, 'getSeasons').and.returnValue(of([]));
    component.getSeasons();
    expect(service.getSeasons).toHaveBeenCalled();
  });

  it('window.open should be called in navigateOut', () => {
    const fakeSeason = {url: 'fake-url'} as Season;
    spyOn(window, 'open').withArgs(fakeSeason.url, '_blank');
    component.navigateOut(fakeSeason);
    expect(window.open).toHaveBeenCalledWith(fakeSeason.url, '_blank');
  });

  it('if ergastService.getSeasons failed, should show appropriate message to user', () => {
    spyOn(service, 'getSeasons').and.callFake(() => {
      return throwError('');
    });
    component.getSeasons();
    expect(component.seasons.errorMessage).toBe('Failed to load sessions');
  });
});
