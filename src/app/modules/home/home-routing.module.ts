import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {PageNotFoundComponent} from '../../core/shared/page-not-found/page-not-found.component';
import {SeasonsComponent} from './seasons/seasons.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'home',
        component: SeasonsComponent,
      },
      {
        path: 'winners/:season',
        loadChildren: () => import('../winners/winners.module').then(x => x.WinnersModule)
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
