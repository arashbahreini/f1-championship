import {Component, OnInit} from '@angular/core';
import {Season} from '../../core/models/season.model';
import {ErgastService} from '../../core/service/ergast.service';
import {ApiResult} from '../../core/models/api-result.model';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {faWikipediaW} from '@fortawesome/free-brands-svg-icons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  ngOnInit(): void {
  }
}
