import {Component, OnInit} from '@angular/core';
import {ErgastService} from '../../core/service/ergast.service';
import {Race, RaceResult} from '../../core/models/race.model';
import {ApiResult} from '../../core/models/api-result.model';
import {ActivatedRoute} from '@angular/router';
import {Standing} from '../../core/models/standing.model';

@Component({
  selector: 'app-winners',
  templateUrl: './winners.component.html',
  styleUrls: ['./winners.component.scss']
})
export class WinnersComponent implements OnInit {

  races: ApiResult<Race[]> = new ApiResult<Race[]>();
  season: string;

  constructor(
    private ergastService: ErgastService,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.season = this.activatedRoute.snapshot.paramMap.get('season');
    if (!this.season || (this.season < '2005' || this.season > '2014')) {
      this.races.setError('Date is out of range');
      return;
    }
    this.getRaces();
  }

  // To get race list
  getRaces(): void {
    this.races.setLoading(true);
    this.ergastService.getRaces(this.season).subscribe((res: Race[]) => {
      if (res && res.length) {
        // To get all winners list
        this.getWinners(res);
      }
    }, () => {
      this.races.setError('Failed to load winner information');
    });
  }

  getWinners(races: Race[]): void {
    this.ergastService.getWinners().subscribe((res: Standing[]) => {
      if (!res) {
        this.races.setError('Information about this session cannot be found.');
        return;
      }

      const targetSession = res.find(x => x.season === this.season);
      if (!targetSession) {
        this.races.setError('Information about this session cannot be found.');
        return;
      }
      const winnerOfTheSession = targetSession.DriverStandings[0];

      const wonRace = races.find(x => x.Results[0].Driver.driverId === winnerOfTheSession.Driver.driverId);
      // Mapping the winners and races and then trigger isSessionWinner of the season's winner
      wonRace.Results[0].Driver.isSessionWinner = true;
      this.races.setData(races);
      this.races.setLoading(false);
    }, () => {
      this.races.setError('Failed to load winner information');
    });
  }

}
