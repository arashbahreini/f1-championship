import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WinnersComponent} from './winners.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FontModule} from '../common/font/font.module';
import {ActivatedRoute} from '@angular/router';
import {Observable, of, throwError} from 'rxjs';
import {Driver, Race, RaceResult} from '../../core/models/race.model';
import {Season} from '../../core/models/season.model';
import {ErgastService} from '../../core/service/ergast.service';
import {Standing} from '../../core/models/standing.model';

class MockErgastService {
  getRaces(season: Season): Observable<Race[]> {
    return of([] as Race[]);
  }

  getWinners(): Observable<Standing[]> {
    return of([] as Standing[]);
  }
}

describe('WinnersComponent', () => {
  let component: WinnersComponent;
  let fixture: ComponentFixture<WinnersComponent>;
  let route: any;
  let service: ErgastService;

  let fakeWinnersResult: Standing[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        FontModule],
      declarations: [WinnersComponent],
      providers: [
        {
          provide: ErgastService,
          useClass: MockErgastService
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinnersComponent);
    component = fixture.componentInstance;

    // from v9.0.0 use TestBed.inject  instead of TestBed.get
    route = TestBed.inject(ActivatedRoute);

    service = fixture.debugElement.injector.get(ErgastService);
    fixture.detectChanges();
  });

  afterEach(() => {
    // To have new instance of component after each test
    // Then tests will not affect each other
    fixture = TestBed.createComponent(WinnersComponent);
    component = fixture.componentInstance;
    fakeWinnersResult = [
      {
        season: '2010',
        round: '1',
        DriverStandings: [{
          Driver: {
            driverId: 'ID123'
          } as Driver
        }] as RaceResult[]
      }
    ] as Standing[];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Get route parameter in onInit method', () => {
    // Set fake route param to 2005 and call ngOnInit()
    // The component.session value should be set to 2005
    spyOn(route.snapshot.paramMap, 'get').and.returnValue('2005');
    spyOn(component, 'getRaces').and.callFake(() => {

    });
    component.ngOnInit();
    expect(component.season).toBe('2005');
  });

  it('getRaces should be called in onInit', () => {
    spyOn(route.snapshot.paramMap, 'get').and.returnValue('2010');
    spyOn(component, 'getRaces');
    component.ngOnInit();
    expect(component.getRaces).toHaveBeenCalled();
  });

  it('races.setError should be called if the year is out of range', () => {
    spyOn(route.snapshot.paramMap, 'get').and.returnValue('2000');
    spyOn(component.races, 'setError');
    component.ngOnInit();
    expect(component.races.setError).toHaveBeenCalledWith('Date is out of range');
  });

  it('getRaces method should call ergastService.getRaces', () => {
    spyOn(service, 'getRaces').and.returnValue(of([]));
    component.getRaces();
    expect(service.getRaces).toHaveBeenCalled();
  });

  it('getWinners should be called with ergastService.getRaces response', () => {
    const fakeResponse = of([{}] as Race[]);
    spyOn(service, 'getRaces').and.returnValue(fakeResponse);
    spyOn(component, 'getWinners');
    spyOn(service, 'getWinners').and.returnValue(of([]));
    component.getRaces();
    fakeResponse.subscribe((res: Race[]) => {
      expect(component.getWinners).toHaveBeenCalledWith(res);
    });
  });

  it('getWinners method should call ergastService.getWinners', () => {
    spyOn(service, 'getWinners').and.returnValue(of(fakeWinnersResult));
    component.getWinners([]);
    expect(service.getWinners).toHaveBeenCalled();
  });

  it('getWinners method should trigger isSessionWinner to true for session winner', () => {
    component.season = '2010';
    const fakeRaces = [
      {
        Results: [
          {
            Driver: {
              isSessionWinner: false,
              driverId: 'ID123'
            } as Driver
          }
        ] as RaceResult[]
      }] as Race[];

    spyOn(service, 'getWinners').and.returnValue(of(fakeWinnersResult));
    component.getWinners(fakeRaces);
    service.getWinners().subscribe((res) => {
      expect(fakeRaces[0].Results[0].Driver.isSessionWinner).toBeTrue();
    });
  });

  it('if ergastService.getRaces failed, should show appropriate message to user', () => {
    spyOn(service, 'getRaces').and.callFake(() => {
      return throwError('');
    });
    component.getRaces();
    expect(component.races.errorMessage).toBe('Failed to load winner information');
  });

  it('if ergastService.getWinners failed, should show appropriate message to user', () => {
    spyOn(service, 'getWinners').and.callFake(() => {
      return throwError('');
    });
    component.getWinners([]);
    expect(component.races.errorMessage).toBe('Failed to load winner information');
  });

  it('if ergastService.getWinners failed, should show appropriate message to user', () => {
    spyOn(service, 'getWinners').and.callFake(() => {
      return of(null);
    });
    component.getWinners([]);
    expect(component.races.errorMessage).toBe('Information about this session cannot be found.');
  });
});
