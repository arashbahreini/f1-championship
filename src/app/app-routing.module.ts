import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PageNotFoundComponent} from './core/shared/page-not-found/page-not-found.component';

const routes: Routes = [
  // To load home module lazily
  {
    path: '',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent,
  },
  // if user write any url which is out of the url list, then he navigate to /page-not-found page
  {
    path: '**',
    redirectTo: 'page-not-found',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
