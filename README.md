# F1 Championship
# Getting Started
This instruction will give you a copy of the project and help you run it on your local machine and/or web server for development and testing purpose.

## Requirements

Before you start installing the project, you should install below items in your machine:

* [NodeJs](https://nodejs.org/en/)
* @angular/cli >= 10 (```npm install -g @angular/cli```)

### Download and install the project
###### open cmd and run the following commands
1. ``git clone https://arashbahreini@bitbucket.org/arashbahreini/f1-championship.git``
2. ``cd .\f1-championship\``
3. ``npm install``
4. ``npm run start``

## Run test
###### For running unit tests execute
Run ``npm run test:local`` which actually runs ``ng test --code-coverage``
  
Note: Tests run on ```Karma``` which has set up in ```Karma.conf.js``` file.

Note: code coverage generate coverage index and in this project **100%** of 
code is covered by unit test.
###### For running e2e test execute
Run ``npm run e2e`` which actually runs ``ng e2e`` command and open browser 
then execute end to end tests of the application

Note: before running e2e tests you should upgrade your Google chrome to the latest 
version. open ``chrome://settings/help`` in your Google chrome.
The expected version is > 86

Note: e2e will host on port 4200, so before 
running e2e command you should stop ``npm run start`` command

## Build application
Run ```npm run build``` build the application in un-minified format 
and  ```npm run build:prod``` build application in human unreadable, minified and in --prod version


## PipeLine | Automation
Automation is design in [bitbucket-pipelines.yml](./bitbucket-pipelines.yml) file
and it is a simple pipeline, in order check lint, unit test and finally build 
application in production and AOT way. 
Note: The output of this application is not going to deploy to any server, so last
step of pipeline just generate output in ``dist`` folder.

[![Pipelines](https://img.shields.io/badge/Bitbucket-Pipelines-blue.svg?style=flat)](https://bitbucket.org/arashbahreini/f1-championship/addon/pipelines/home#!/) 

## External dependencies
#### This project has below external dependencies
* [Bootstrap](https://getbootstrap.com/)
* [Font-awesome](https://fontawesome.com/)


### Notes and best practices
* If you open the [root](http://localhost:4200/) of application, you will see the movement animation
* A page not found component is designed, and it appears if user add an unknown route 
* Class and interfaces are named according to the best practices
* In the winner page if you change the route parameter and set it to something < 2005 or > 2014, then 
and error message appear to say: the session is out of range
* For every place that an API is being called, an animation loading appears, so user does not feel like
site is processing his needs
* Every part of the application which is calling an API, is watched by 
error handler, so if any API fail, an appropriate message would be appeared
 for user
* both the pages are covered by ``end to end`` test
* I used Angular animation to creating more **user-friendly** and **eye appealing** 
pages
* Code coverage for unit test is **100%**

## Authors

* **Arash Bahreini**

See also my [GitHub](https://github.com/arashbahreini) profile.
