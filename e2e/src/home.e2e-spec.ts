import {AppPage} from './app.po';
import {browser, by, element, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('should display Welcome to greatest F1 championship world message', () => {
    expect(element(by.css('.greeting-text')).getText()).toEqual('Welcome to greatest F1 championship world');
  });

  it('should display 10 boxed for years', () => {
    const elements = element.all(by.css('.season-container'));
    browser.sleep(3000);
    expect(elements.count()).toBe(10);
  });

  it('should display 2005 in the first box for seasons', () => {
    const expectedElement = element(by.css('#season_2008'));
    browser.sleep(3000);
    expect(expectedElement.getText()).toBe('2008');
  });

  it('should redirect to winners page on clicking on the winners button', () => {
    browser.sleep(3000);
    const button = element(by.css('#btn_show_winner_2008')).click();
    browser.sleep(1000);
    browser.getCurrentUrl().then((actualUrl: string) => {
      expect(actualUrl.indexOf('winners') > -1).toBeTruthy();
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
