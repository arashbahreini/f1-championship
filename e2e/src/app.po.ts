import {browser} from 'protractor';

export class AppPage {
  navigateTo(url?: string): Promise<unknown> {
    url = url ? (browser.baseUrl + url) : browser.baseUrl;
    return browser.get(url) as Promise<unknown>;
  }
}
