import {AppPage} from './app.po';
import {browser, by, element, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo('winners/2006');
  });

  it('should display List of winners message', () => {
    expect(element(by.css('.greeting-text')).getText()).toEqual('List of winners');
  });

  it('should display Back to main button', () => {
    expect(element(by.css('.back-arrow-container')).isPresent).toBeTruthy();
  });

  it('should present main div to contain table', () => {
    expect(element.all(by.css('.result-container')).isPresent).toBeTruthy();
  });

  it('should go to home page when Back to main button getting hit', () => {
    const button = element.all(by.css('#btn_back_to_main'));
    button.click();
    browser.sleep(1000);
    browser.getCurrentUrl().then((actualUrl: string) => {
      expect(actualUrl.indexOf('home') > -1).toBeTruthy();
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
